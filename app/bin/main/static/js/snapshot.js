$('#formCamera').submit(function() {
	
    console.info('スナップショットをとるよ！');
    var videoElement = document.querySelector('video');
    var canvasElement = document.querySelector('canvas');
    var context = canvasElement.getContext('2d');

    context.drawImage(videoElement, 0, 0, videoElement.width, videoElement.height);
    document.querySelector('img').src = canvasElement.toDataURL('image/webp');

    var img_camera = canvasElement.toDataURL('image/webp');

    var var1 = document.getElementById('image');
    var1.value = img_camera;
    
    console.info(img_camera);
    
    $.ajax({
    	type : "POST", 
        data : img_camera,
        processData : false,
        contentType: false,
    });
})
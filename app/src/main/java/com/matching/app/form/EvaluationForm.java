package com.matching.app.form;

import java.io.Serializable;

public class EvaluationForm implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String title;
	private String evaluation;
	private String hyouka;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getEvaluation() {
		return evaluation;
	}
	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}
	public String getHyouka() {
		return hyouka;
	}
	public void setHyouka(String hyouka) {
		this.hyouka = hyouka;
	}
}

package com.matching.app.form;

import java.io.Serializable;
import java.util.Date;

/**
 * 登録画面Form
 */
public class RegistrateForm implements Serializable {

	private static final long serialVersionUID = 1L;

	private String title;
	private String category;
	private String detail;
	private Date deadline;
	private String cost;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public Date getDeadline() {
		return deadline;
	}
	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
}

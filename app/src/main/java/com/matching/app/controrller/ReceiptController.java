package com.matching.app.controrller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.matching.app.entity.Account;
import com.matching.app.entity.Task;
import com.matching.app.service.TaskService;

/**
 * 受領用のController
 */
@Controller
@RequestMapping(path = "/receipt")
public class ReceiptController {

	@Autowired
	TaskService taskService;
	
//	@GetMapping(path = "/{id}")
	@PostMapping
	public ModelAndView receipt(ModelAndView modelAndView, @AuthenticationPrincipal Account account) {
		// TODO
		Task task = new Task();
		task.setId(104);
		taskService.receipt(task, account);
		
		modelAndView.setViewName("receipt");
		return modelAndView;
	}
}

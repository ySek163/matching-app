package com.matching.app.controrller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.matching.app.entity.Account;
import com.matching.app.entity.Task;
import com.matching.app.service.TaskService;

/**
 * topページ
 */
@Controller
public class IndexController {

	@Autowired
	TaskService taskService;
	
	@RequestMapping(path = "/index")
	public ModelAndView index(ModelAndView modelAndView, @AuthenticationPrincipal Account account) {
		
		// タスク一覧
		List<Task> taskList = taskService.getTaskAll();

		// 受領中
		List<Task> receiptTaskList = taskService.getReceiptTaskList(account.getAccountId());
		
		// 依頼中
		List<Task> requestTaskList = taskService.getRequestTaskList(account.getAccountId());
		
		// オススメ
		
		modelAndView.addObject("taskList", taskList);
		modelAndView.addObject("receiptTaskList", receiptTaskList);
		modelAndView.addObject("requestTaskList", requestTaskList);
		modelAndView.setViewName("index");
		
		return modelAndView;
	}
	
	/**
	 * 後で削除
	 */
	@RequestMapping(path = "/indexBk")
	public String indexBk() {
		return "index_bk";
	}
}

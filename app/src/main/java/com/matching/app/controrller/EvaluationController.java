package com.matching.app.controrller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.matching.app.entity.Account;
import com.matching.app.entity.Task;
import com.matching.app.form.EvaluationForm;
import com.matching.app.service.ImageService;
import com.matching.app.service.TaskService;
import com.matching.app.service.TextService;
import com.matching.app.service.TranslatorService;

@Controller
@RequestMapping(path = "/evaluation")
public class EvaluationController {

	@Autowired
	TaskService taskService;
	
	@Autowired
	TextService textService;
	
	@Autowired
	ImageService imageService;
	
	// TODO 後で削除
	@Autowired
	TranslatorService translatorService;
	
	@GetMapping(path = "/{id}")
	public ModelAndView evaluetion(@PathVariable Integer id, ModelAndView modelAndView) throws IOException {

		Task result = taskService.getTask(id);
		
		EvaluationForm form = new EvaluationForm();
		form.setId(id);
		form.setTitle(result.getTitle());

		modelAndView.addObject("evaluationForm", form);
		modelAndView.setViewName("evaluation");
		return modelAndView;
	}
	
	@PostMapping
	public ModelAndView postEvaluation(HttpServletRequest request, ModelAndView modelAndView, @ModelAttribute(name = "evaluationForm") EvaluationForm evaluationForm) throws IOException {
				
		Double radioResult = radioResult(evaluationForm.getHyouka());
		
		String evaluationText = TranslatorService.prettify(translatorService.translator(evaluationForm.getEvaluation()));		
		Double textResult = textService.analysis(evaluationText);
		
//		Double imageResult = imageService.analysis();
		Double imageResult = 0.0;
		
		modelAndView.addObject("radio_message", radioResult);
		modelAndView.addObject("text_message", textResult);
		
		if (evaluationForm.getId() == 104) {
			imageResult = imageService.analysis(evaluationForm.getId());
			modelAndView.addObject("image_path", "image/IMG_9347.JPG");
		} else {
			imageResult = imageService.analysis(evaluationForm.getId());
			modelAndView.addObject("image_path", "image/IMG_9348.JPG");
		}
		
		modelAndView.addObject("image_message", imageResult);
		modelAndView.setViewName("textResult");
	
		Task task = new Task();
		task.setId(evaluationForm.getId());
		Account account = new Account();
		account.setAccountId("PITXXXXX");
		
		taskService.receipt(task, account);
		
		return modelAndView;
	}
	
	private Double radioResult(String date) {
		
		Double result = 0.0;
		
		switch (date) {
		case "veryGood":
			result = 50.0;
			break;
		case "good":
			result = 40.0;
			break;
		case "normal":
			result = 30.0;
			break;
		case "bad":
			result = 20.0;
			break;
		case "veryBad":
			result = 10.0;
			break;
		}
		return result;
	}
}

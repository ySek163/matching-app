package com.matching.app.controrller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.matching.app.entity.Account;
import com.matching.app.repository.AccountRepository;

/**
 * Accountテーブルにデータを挿入するためのクラス
 * 後で削除
 */
@RestController
@RequestMapping(path = "/account")
public class AccountRestController {

	@Autowired
	AccountRepository accountRepository;

	@GetMapping
	public String save() {
		Account insertData = new Account();
		insertData.setAccountId("PIT02121");
		insertData.setName("山田 太郎");
		insertData.setPassword("pass");
		accountRepository.save(insertData);
		return "OK!";
	}
}

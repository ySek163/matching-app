package com.matching.app.controrller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.matching.app.form.TextForm;
import com.matching.app.service.ImageService;
import com.matching.app.service.TextService;

/**
 * テキスト感情分析Controller
 */
@Controller
@RequestMapping(path = "text")
public class TextController {

	@Autowired
	private TextService textService;
	
	@Autowired
	private ImageService imageService;
	
	@GetMapping
	public ModelAndView view(ModelAndView modelAndView) {
		modelAndView.setViewName("textInput");
		return modelAndView;
	}
	
	@PostMapping
	public ModelAndView analysis(@ModelAttribute(name = "textForm") TextForm textForm , ModelAndView modelAndView) {
		
		Double textResult = textService.analysis(textForm.getText());	
		modelAndView.addObject("text_message", textResult);
		
		Double imageResult = imageService.analysis(104);
//		modelAndView.addObject("image_message", TextService.prettify(textResult));
		modelAndView.addObject("image_message", imageResult);
		
		modelAndView.setViewName("textResult");
		
		return modelAndView;
	}
}

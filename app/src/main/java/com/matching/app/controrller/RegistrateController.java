package com.matching.app.controrller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.matching.app.entity.Account;
import com.matching.app.entity.Task;
import com.matching.app.form.RegistrateForm;
import com.matching.app.service.TaskService;

/**
 * タスク登録
 */
@Controller
@RequestMapping(path = "/registrate")
public class RegistrateController {

	@Autowired
	TaskService registrateService;
	
	/**
	 * タスク登録画面表示
	 */
	@GetMapping
	public ModelAndView getRegistrate(ModelAndView modelAndView) {
		modelAndView.setViewName("registrate");

		return modelAndView;
	}
	
	/**
	 * タスク登録
	 */
	@PostMapping
	public ModelAndView registrate(ModelAndView modelAndView, @ModelAttribute(name = "registrateForm") RegistrateForm registrateForm, @AuthenticationPrincipal Account account) {

		Task task = new Task();
		BeanUtils.copyProperties(registrateForm, task);
		task.setRequestUserId(account.getAccountId());
		registrateService.registrate(task);
		
		modelAndView.setViewName("complate");
		
		return modelAndView;
	}
}

package com.matching.app.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;


@Service
public class TextService {

	public Double analysis(String text) {
		String response = null;
		try {
			Documents documents = new Documents();
			documents.add("1", "en", text);
			response = GetSentiment(documents);
		} catch (Exception e) {
			System.out.println(e);
		}
		return prettify(response);
	}

	// Replace the accessKey string value with your valid access key.
	static String accessKey = "2914dee8c6604b958664850c548a842e";

	// Replace or verify the region.

	// You must use the same region in your REST API call as you used to obtain your
	// access keys.
	// For example, if you obtained your access keys from the westus region, replace
	// "westcentralus" in the URI below with "westus".

	// NOTE: Free trial access keys are generated in the westcentralus region, so if
	// you are using
	// a free trial access key, you should not need to change this region.
	static String host = "https://japaneast.api.cognitive.microsoft.com";

	static String path = "/text/analytics/v2.0/sentiment";

	public static String GetSentiment(Documents documents) throws Exception {
		String text = new Gson().toJson(documents);
		byte[] encoded_text = text.getBytes("UTF-8");

		URL url = new URL(host + path);
		HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "text/json");
		connection.setRequestProperty("Ocp-Apim-Subscription-Key", accessKey);
		connection.setDoOutput(true);

		DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
		wr.write(encoded_text, 0, encoded_text.length);
		wr.flush();
		wr.close();

		StringBuilder response = new StringBuilder();
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String line;
		while ((line = in.readLine()) != null) {
			response.append(line);
		}
		in.close();

		return response.toString();
	}

	public static Double prettify(String json_text) {

		JSONObject jsonObject = new JSONObject(json_text);
		JSONArray jsonArray = (JSONArray)jsonObject.get("documents");
		Double score = jsonArray.getJSONObject(0).getDouble("score");
		score = score * 100 ;

//		JsonParser parser = new JsonParser();
//		JsonObject json = parser.parse(json_text).getAsJsonObject();
//		Gson gson = new GsonBuilder().setPrettyPrinting().create();

//		ResultElements resultElements = gson.fromJson(json.toString(), ResultElements.class);
//		System.out.println("****" + resultElements.getResultElements());

		return Math.ceil(score);
	}

	class Document {
		public String id, language, text;

		public Document(String id, String language, String text) {
			this.id = id;
			this.language = language;
			this.text = text;
		}
		
	}

	class Documents {
		public List<Document> documents;

		public Documents() {
			this.documents = new ArrayList<Document>();
		}

		public void add(String id, String language, String text) {
			this.documents.add(new Document(id, language, text));
		}
	}
	
	class ResultElements {
		public List<ResultElement> resultElements;
		
		public ResultElements() {
			this.resultElements = new ArrayList<ResultElement>();
		}
		
		public void add(String id, String score) {
			this.resultElements.add(new ResultElement(id, score));
		}
		
		public List<ResultElement> getResultElements() {
			return resultElements;
		}
	}
	
	class ResultElement {
		public String id, score;

		public ResultElement(String id, String score) {
			super();
			this.id = id;
			this.score = score;
		}
		
		public String getScore() {
			return score;
		}
	}
}

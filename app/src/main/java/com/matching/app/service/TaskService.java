package com.matching.app.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matching.app.entity.Account;
import com.matching.app.entity.Task;
import com.matching.app.repository.TaskRepository;

/**
 * Task Service
 */
@Service
public class TaskService {

	@Autowired
	TaskRepository taskRepository;

	/**
	 * タスク一覧取得
	 */
	public List<Task> getTaskAll() {
		return taskRepository.findAll();
	}
	
	/**
	 * タスク登録
	 */
	public void registrate(Task task) {
		task.setState(State.DURING_REQUEST);
		taskRepository.save(task);
	}
	
	/**
	 * タスク受領
	 */
	public void receipt(Task task, Account account) {
		Task result = taskRepository.findById(task.getId()).get();
		result.setReceiptUserId(account.getAccountId());
		taskRepository.save(result);
	}
	
	/**
	 * タスク一件取得
	 */
	public Task getTask(Integer id) {
		return taskRepository.findById(id).get();
	}
	
	/**
	 * 受領中タスク一覧取得
	 */
	public List<Task> getReceiptTaskList(String receiptUserId) {
		return taskRepository.findAllByReceiptUserId(receiptUserId);
	}
	
	/**
	 * 依頼中タスク一覧取得
	 */
	public List<Task> getRequestTaskList(String requestUserId) {
		return taskRepository.findAllByRequestUserId(requestUserId);
	}
	
	/**
	 * オススメタスク一覧取得
	 */
	public List<Task> getRecommendedTaskList() {
		// TODO オススメ判定でAPI呼び出し
		
		// TODO 一旦からのインスタンスを返す
		return new ArrayList<Task>();
	}
}

package com.matching.app.service;

/**
 * State
 */
public enum State {

	DURING_REQUEST("1", "依頼中"),
	REQUESTED("2", "済");
	
	private String id;
	private String name;
	
	private State(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return id;
	}
	public String getName() {
		return name;
	}
}

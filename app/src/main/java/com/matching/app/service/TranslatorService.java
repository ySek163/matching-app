package com.matching.app.service;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

/**
 * 翻訳サービス
 */
@Service
public class TranslatorService {

	String subscriptionKey = "c37d78d74f39497db670462ba2b24a53";
	String url = "https://api.cognitive.microsofttranslator.com/translate?api-version=3.0&to=en&scope=translation";
	
	public String translator(String text) throws IOException {
		
		// Instantiates the OkHttpClient.
		OkHttpClient client = new OkHttpClient();
		
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, 
				"[{\n\t\"Text\": \"" + text + "\"\n}]");
		
		Request request = new Request.Builder().url(url).post(body)
				.addHeader("Ocp-Apim-Subscription-Key", subscriptionKey).addHeader("Content-type", "application/json")
				.build();
		Response response = client.newCall(request).execute();
		return response.body().string();
		
	}
	
	// This function prettifies the json response.
	public static String prettify(String json_text) {

		JSONArray jsonArray = new JSONArray(json_text);
		JSONObject jsonObject = (JSONObject) jsonArray.getJSONObject(0);
		JSONArray jsonArray2 = (JSONArray) jsonObject.get("translations");
		
		return jsonArray2.getJSONObject(0).getString("text");
		
//	    JsonParser parser = new JsonParser();
//	    JsonElement json = parser.parse(json_text);
//	    Gson gson = new GsonBuilder().setPrettyPrinting().create();
//	    return gson.toJson(json);
	    
	}
}

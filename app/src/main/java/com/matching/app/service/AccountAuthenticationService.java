package com.matching.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.matching.app.entity.Account;
import com.matching.app.repository.AccountRepository;

@Service
public class AccountAuthenticationService implements UserDetailsService {

	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Override
	public Account loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = accountRepository.findByAccountId(username);
		account.setPassword(passwordEncoder.encode(account.getPassword()));
		
		// TODO 例外処理
		
		return account;
	}
}

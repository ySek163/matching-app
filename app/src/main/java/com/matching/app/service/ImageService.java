package com.matching.app.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class ImageService {

	// Replace <Subscription Key> with your valid subscription key.
	private static final String subscriptionKey = "8c3871dde02345c795c07bcec233601c";

	// NOTE: You must use the same region in your REST call as you used to
	// obtain your subscription keys. For example, if you obtained your
	// subscription keys from westus, replace "westcentralus" in the URL
	// below with "westus".
	//
	// Free trial subscription keys are generated in the "westus" region. If you
	// use a free trial subscription key, you shouldn't need to change this region.
	private static final String uriBase = "https://japaneast.api.cognitive.microsoft.com/face/v1.0/detect";

	private static final String imageWithFacesA = "{\"url\":\"https://gitlab.com/ySek163/matching-app/raw/master/app/src/main/resources/static/image/IMG_9347.jpg\"}";
	private static final String imageWithFacesB = "{\"url\":\"https://gitlab.com/ySek163/matching-app/raw/master/app/src/main/resources/static/image/IMG_9348.JPG\"}";

	private static final String faceAttributes = "age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise";

	public Double analysis(Integer id) {
				
		HttpClient httpclient = new DefaultHttpClient();
		
		Double result = 0.0;
		try {
			URIBuilder builder = new URIBuilder(uriBase);

			// Request parameters. All of them are optional.
			builder.setParameter("returnFaceId", "true");
			builder.setParameter("returnFaceLandmarks", "false");
			builder.setParameter("returnFaceAttributes", faceAttributes);

			// Prepare the URI for the REST API call.
			URI uri = builder.build();
			HttpPost request = new HttpPost(uri);

			// Request headers.
			request.setHeader("Content-Type", "application/json");
			request.setHeader("Ocp-Apim-Subscription-Key", subscriptionKey);

			// Request body.
			if (id == 104) {
				StringEntity reqEntity = new StringEntity(imageWithFacesA);
				request.setEntity(reqEntity);
			} else {
				StringEntity reqEntity = new StringEntity(imageWithFacesB);
				request.setEntity(reqEntity);
			}

			// Execute the REST API call and get the response entity.
			HttpResponse response = httpclient.execute(request);
			HttpEntity entity = response.getEntity();

			if (entity != null) {

				String jsonString = EntityUtils.toString(entity).trim();
				if (jsonString.charAt(0) == '[') {
					JSONArray jsonArray = new JSONArray(jsonString);
//					System.out.println(jsonArray.toString(2));
//					result = jsonArray.toString(2);
					JSONObject object = (JSONObject)jsonArray.getJSONObject(0).get("faceAttributes");
					result = decision(object);

				} else if (jsonString.charAt(0) == '{') {
					JSONObject jsonObject = new JSONObject(jsonString);
//					System.out.println(jsonObject.toString(2));
				} else {
//					System.out.println(jsonString);
				}
			}
		} catch (Exception e) {
			// Display error message.
			System.out.println(e.getMessage());
		}
		return result;
	}
	
	public Double decision(JSONObject json) {
		
		List<String> elementList = new ArrayList<String>() {
			{
				add("contempt");	 	// 侮辱		-10
				add("surprise"); 		// 驚き		0
				add("happiness");	// 幸福		100
				add("neutral");			// 自然		10
				add("sadness");		// 悲しみ	-10
				add("disgust");			// 嫌悪		-50
				add("anger");			// 怒り			-10
				add("fear");				// 恐れ		-10
			}
		};
		
		Double result = 0.0;
		for (String element : elementList) {
			Double target = json.getJSONObject("emotion").getDouble(element);

			switch (element) {
			case "contempt":
				Double double1 = target * -10;
				result = result + double1;
				break;
			case "surprise":
				Double double2 = target * 0;
				result = result + double2;
				break;
			case "happiness":
				Double double3 = target * 100;
				result = result + double3;
				break;
			case "neutral":
				Double double4 = target * 10;
				result = result + double4;
				break;
			case "sadness":
				Double double5 = target * -10;
				result = result + double5;
				break;
			case "disgust":
				Double double6 = target * -50;
				result = result + double6;
				break;
			case "anger":
				Double double7 = target * -10;
				result = result + double7;
				break;
			case "fear":
				Double double8 = target * -10;
				result = result + double8;
				break;
			}
		}
		return result;
	}
}

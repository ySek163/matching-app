package com.matching.app.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.matching.app.service.State;

/**
 * Task Entity
 */
@Entity
@Table(name = "task")
public class Task {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String title;
	private String category;
	private String detail;
	private Date deadline;
	private String cost;
	@Enumerated(EnumType.STRING)
	private State state;
	// 受領者
	private String receiptUserId;
	// 依頼者
	private String requestUserId;
	private Integer point;
	private Double radioResult;
	private Double textResult;
	private Double imageResult;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public Date getDeadline() {
		return deadline;
	}
	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	public String getReceiptUserId() {
		return receiptUserId;
	}
	public void setReceiptUserId(String receiptUserId) {
		this.receiptUserId = receiptUserId;
	}
	public String getRequestUserId() {
		return requestUserId;
	}
	public void setRequestUserId(String requestUserId) {
		this.requestUserId = requestUserId;
	}
	public Integer getPoint() {
		return point;
	}
	public void setPoint(Integer point) {
		this.point = point;
	}
	public Double getRadioResult() {
		return radioResult;
	}
	public void setRadioResult(Double radioResult) {
		this.radioResult = radioResult;
	}
	public Double getTextResult() {
		return textResult;
	}
	public void setTextResult(Double textResult) {
		this.textResult = textResult;
	}
	public Double getImageResult() {
		return imageResult;
	}
	public void setImageResult(Double imageResult) {
		this.imageResult = imageResult;
	}
}

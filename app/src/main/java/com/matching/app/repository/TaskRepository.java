package com.matching.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.matching.app.entity.Task;

/**
 * Taskテーブル用のRepository
 */
public interface TaskRepository extends JpaRepository<Task, Integer> {
	public List<Task> findAllByRequestUserId(String requestUserId);
	public List<Task> findAllByReceiptUserId(String receiptUserId);
}

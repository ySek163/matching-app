package com.matching.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.matching.app.entity.Account;

/**
 * Userテーブル用のRepository
 */
public interface AccountRepository extends JpaRepository<Account, String> {
	public Account findByAccountId(String id);
}
